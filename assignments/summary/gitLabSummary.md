# Summary

## GitLab

Gitlab is a service that provides remote access to Git repositories. Gitlab is similar to Github and ranks 2nd most popular used git service next to Github. 

## Features

* Hosts private and public repo's for free
* Free platform for managin the Git repo's
* Provides free public and private repo's, issue-tracking and issues
* User friendly web interface.
* Users can discuss about the merge requests, comment and describe about them
* Provides its own **Continuous Integration (CI)** system for managing the projects and provides user interface along with other features of GitLab.