# Summary

## Version Control System

To understand what is GIT first we have to understand what is Version Control System (VCS). Version Control System (VCS) is a software that helps software developers to work together and maintain a complete history of their work.

Advantages of VCS:

* Allows developers to work simultaneously.
* No conflicts between each other's changes.
* Maintains a history of every change you.

Now that we know what is VCS, we can get a good understanding of GIT.

GIT is a version-control system for tracking changes in computer files and coordinating work on those files among multiple people. Git is a **Distributed Version Control System**.

## GIT

TO work around with GIT, it is important to understand some terminology used by GIT.

* **WORKING DIRECTORY** The workspace you are currently working on.
* **STAGING AREA** The changes are staged/grouped to be commited to the local repo.
* **LOCAL REPO** The commited change will be updated here.
* **WORKING DIRECTORY** The remote branch where everybody in your team can see the changes made by you.

![Git Reference Image](gitref.png)

Now we can dive into some git commands which helps us manage files and folders.

1. **git add** To update a file in the working directory to the staging area.
2. **git commit** To update a file in staging area to the local repo.
3. **git push** To update a file in the local repo to remote branch.
4. **git fetch** To get files from the remote repo to the local repo but not in the working directory.
5. **git merge** To get the files from the local repository into the working directory
6. **git pull** To get the files from the remote repo directly to the working directory. Equivalent to **git fetch** + **git merge**